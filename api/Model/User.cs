﻿using SqlSugar;

namespace WebApplication1.Model
{
    public class User
    {
        [SugarColumn(IsPrimaryKey = true)]//使用sqlSugar：配置主键
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Address { get; set; }
        public int Order { get; set; }

    }
}
