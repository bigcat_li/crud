import axios from 'axios'
import UserDto  from '../class/UserDto'

// 查询
export const getList=(req:UserDto)=>{
    return axios.post("/api/list",req)
}
// app.MapPost("/list", (Model req) =>
// {
//     return SqlSugarHelper.GetUsers(req);
// });
// 新增
export const add=(req:{})=>{
    return axios.post("/api/add",req)
}
// app.MapPost("/add", (AddReq req) =>
// {
//     return SqlSugarHelper.Add(req);
// });
// 修改
export const edit=(req:{})=>{
    return axios.post("/api/edit",req)
}
// app.MapPost("/edit", (User req) =>
// {
//     return SqlSugarHelper.Edit(req);
// });
// 删除
export const del=(ids:string)=>{
    return axios.get("/api/del?ids="+ids)
}
// app.MapDelete("/delete", (string ids) => 
// {
//     return SqlSugarHelper.Del(ids);
// });