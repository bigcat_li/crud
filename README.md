# ASP.NET Core列表增删改查
![输入图片说明](www/src/assets/Snipaste_2024-01-18_17-14-00.png)
#### 介绍
asp.net core列表的增删改查，www是前端代码，api是后端文件夹，数据库是在后端生成的(改一下数据库连接成本地的数据库进行了)
前端：vue3+vite+element-plus
后端：.net

#### 软件架构
数据库是在后端生成的(改一下数据库连接成本地的数据库进行了)


#### 安装教程
完整的运行教程：

将前端使用vscode ->npm run dev
后端使用vsstudio->配置本地的数据库连接之后，直接run即可

#### 使用说明
数据库是在后端生成的(改一下数据库连接成本地的数据库进行了)


#### 查看教程文档
https://blog.csdn.net/qq_57676486/article/details/135622222?spm=1001.2014.3001.5501


#### 初始化vue连接
![输入图片说明](https://foruda.gitee.com/images/1705569020380560732/5e68f4ae_10792891.png "屏幕截图")

代码链接：https://gitee.com/bigcat_li/vue-initialization

